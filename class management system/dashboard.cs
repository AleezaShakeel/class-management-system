﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FontAwesome.Sharp;

namespace class_management_system
{
    public partial class dashboard : Form
    {
        // fields
        private IconButton currentbtn;
        private Panel leftborderbtn;
        private Form currentchildform;

        // constructor

        public dashboard()
        {
            InitializeComponent();
            leftborderbtn = new Panel();
            leftborderbtn.Size = new Size(7, 60);
            panelmenu.Controls.Add(leftborderbtn);
            // form
            this.Text = string.Empty;
            this.ControlBox = false;
            this.DoubleBuffered = true;
            this.MaximizedBounds = Screen.FromHandle(this.Handle).WorkingArea;
        }
        // structure

            private struct RGBColors
            {
            public static Color color1 = Color.FromArgb(172, 126, 241);
            public static Color color2 = Color.FromArgb(249, 118, 176);
            public static Color color3 = Color.FromArgb(253, 138, 114);
            public static Color color4 = Color.FromArgb(95, 77, 221);
            public static Color color5 = Color.FromArgb(249, 88, 155);
            public static Color color6 = Color.FromArgb(24, 161, 251);
           // public static Color color7 = Color.FromArgb(256, 16, 160);


            }
        //methods
        private void activebtn(object senderbtn,Color color)
        {
            if (senderbtn != null)
            {
                disablebutton();
                //button
                currentbtn = (IconButton)senderbtn;
                currentbtn.BackColor = Color.FromArgb(37, 36, 81);
                currentbtn.ForeColor = color;
                currentbtn.TextAlign = ContentAlignment.MiddleCenter;
                currentbtn.IconColor = color;
                currentbtn.TextImageRelation = TextImageRelation.TextBeforeImage;
                currentbtn.ImageAlign = ContentAlignment.MiddleRight;

                // left border button
                leftborderbtn.BackColor = color;
                leftborderbtn.Location = new Point(0,currentbtn.Location.Y);
                leftborderbtn.Visible = true;
                leftborderbtn.BringToFront();
                //Icon current child form
                iconcurrentchild.IconChar = currentbtn.IconChar;
                iconcurrentchild.IconColor = color;
                 
            }
        }
        private void disablebutton()
        {
            if (currentbtn != null)
            {
                currentbtn.BackColor = Color.FromArgb(0, 0, 64);
                currentbtn.ForeColor = Color.Silver;
                currentbtn.TextAlign = ContentAlignment.MiddleLeft;
                currentbtn.IconColor = Color.Silver;
                currentbtn.TextImageRelation = TextImageRelation.TextBeforeImage;
                currentbtn.ImageAlign = ContentAlignment.MiddleLeft;
            }
        }

        private void openchildform(Form childform)
        {
            if (currentchildform != null)
            {
                // open only form
                currentchildform.Close();
            }
            currentchildform = childform;
            childform.TopLevel = false;
            childform.FormBorderStyle = FormBorderStyle.None;
            childform.Dock = DockStyle.Fill;
            paneldeskop.Controls.Add(childform);
            paneldeskop.Tag = childform;
            childform.BringToFront();
            childform.Show();
            lblchildform.Text = childform.Text;

        }
        private void IconButton1_Click(object sender, EventArgs e)
        {
            activebtn(sender, RGBColors.color1);
       
        }

        private void IconButton2_Click(object sender, EventArgs e)
        {
            activebtn(sender, RGBColors.color2);
            openchildform(new managestudents());
        }

        private void IconButton3_Click(object sender, EventArgs e)
        {
            activebtn(sender, RGBColors.color3);
            openchildform(new manageattendance());
        }

        private void IconButton4_Click(object sender, EventArgs e)
        {
            activebtn(sender, RGBColors.color4);
            openchildform(new manageclo());
        }

        private void IconButton5_Click(object sender, EventArgs e)
        {
            activebtn(sender, RGBColors.color5);
            openchildform(new manageassessment());
        }

        private void IconButton6_Click(object sender, EventArgs e)
        {
            activebtn(sender, RGBColors.color6);
            openchildform(new markevaluation());
        }

        private void Dashboard_Load(object sender, EventArgs e)
        {

        }

        private void Label1_Click(object sender, EventArgs e)
        {

        }

        private void PictureBox1_Click(object sender, EventArgs e)
        {
            currentchildform.Close();
            Reset();
        }

        private void Reset()
        {
            disablebutton();
            leftborderbtn.Visible = false;
            iconcurrentchild.IconChar = IconChar.Home;
            iconcurrentchild.IconColor = Color.MediumPurple;
            lblchildform.Text = "Home";
        }

        // Drag from
        [DllImport("User32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("User32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hWnd, int wMsg, int wParam, int lParam);

        private void Paneltitlebar_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void IconPictureBox1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void IconPictureBox3_Click(object sender, EventArgs e)
        {
            if (WindowState == FormWindowState.Normal)
                WindowState = FormWindowState.Maximized;
            else
                WindowState = FormWindowState.Normal;
        }

        private void IconPictureBox2_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }

        private void Paneldeskop_Paint(object sender, PaintEventArgs e)
        {

        }

        private void IconButton7_Click(object sender, EventArgs e)
        {
            activebtn(sender, RGBColors.color3);
            openchildform(new reports());
        }
    }
}
