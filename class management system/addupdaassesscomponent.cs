﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace class_management_system
{
    public partial class addupdaassesscomponent : Form
    {
        public addupdaassesscomponent()
        {
            InitializeComponent();
        }

        private void PictureBox1_Click(object sender, EventArgs e)
        {
            this.Hide();
            dashboard c = new dashboard();
            c.Show();
        }

        private void Label3_Click(object sender, EventArgs e)
        {

        }

        private void TableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void Panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void Label19_Click(object sender, EventArgs e)
        {

        }

        private void BunifuThinButton22_Click(object sender, EventArgs e)
        {
            if (materialTextBox1.Text == "")
            {
                label11.Text = "Please enter Assessment Component Name";
            }
            if (materialTextBox3.Text == "")
            {
                label13.Text = "Please enter Total Marks";
            }
            else
            {
                label11.Text = " ";
                label13.Text = " ";
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("Insert into AssessmentComponent values (@Name, @RubricId,@TotalMarks,@DateCreated,@DateUpdated,@AssessmentId)", con);
                cmd.Parameters.AddWithValue("@Name", materialTextBox1.Text);
                cmd.Parameters.AddWithValue("@RubricId",int.Parse(materialComboBox1.Text));
                cmd.Parameters.AddWithValue("@TotalMarks",int.Parse(materialTextBox3.Text));
                cmd.Parameters.AddWithValue("@DateCreated", System.DateTime.Now);
                cmd.Parameters.AddWithValue("@DateUPdated", System.DateTime.Now);
                cmd.Parameters.AddWithValue("@AssessmentId", int.Parse(materialComboBox3.Text));
                cmd.ExecuteNonQuery();
                MessageBox.Show("Successfully saved...");
            }
        }

        private void BunifuThinButton24_Click(object sender, EventArgs e)
        {
            if (materialTextBox5.Text == "")
            {
                label16.Text = "Please enter Assessment Component Name";
            }
            if (materialTextBox7.Text == "")
            {
                label18.Text = "Please enter Total Marks";
            }
            else
            {
                label16.Text = " ";
                label18.Text = " ";

                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("UPDATE AssessmentComponent SET Name=@Name, RubricId=@RubricId,TotalMarks=@TotalMarks,DateUpdated=@DateUpdated,AssessmentId=@AssessmentId WHERE Id=@Id", con);
                con.Close();
                con.Open();
                cmd.Parameters.AddWithValue("@Id", int.Parse(materialComboBox5.Text));
                cmd.Parameters.AddWithValue("@Name", materialTextBox5.Text);
                cmd.Parameters.AddWithValue("@RubricId", int.Parse(materialComboBox2.Text));
                cmd.Parameters.AddWithValue("@TotalMarks", int.Parse(materialTextBox7.Text));
                cmd.Parameters.AddWithValue("@DateUpdated", System.DateTime.Now);
                cmd.Parameters.AddWithValue("@AssessmentId", int.Parse(materialComboBox4.Text));
                cmd.ExecuteNonQuery();
                MessageBox.Show("Successfully Updated...");
            }
           
        }

        private void BunifuThinButton21_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from AssessmentComponent where Id=@Id", con);
            cmd.Parameters.AddWithValue("@Id", int.Parse(materialComboBox5.Text));
            SqlDataReader da = cmd.ExecuteReader();
            while (da.Read())
            {
                materialTextBox5.Text = da.GetValue(1).ToString();
                materialComboBox2.Text = da.GetValue(2).ToString();
                materialTextBox7.Text = da.GetValue(3).ToString();
                materialComboBox4.Text = da.GetValue(6).ToString();
            }
        }

        private void MaterialTextBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar >= 'a' && e.KeyChar <= 'z' || e.KeyChar >= 'A' && e.KeyChar <= 'Z' || e.KeyChar == ' ' || e.KeyChar == 8 || e.KeyChar >= '0' && e.KeyChar <= '9')
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void MaterialTextBox5_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar >= 'a' && e.KeyChar <= 'z' || e.KeyChar >= 'A' && e.KeyChar <= 'Z' || e.KeyChar == ' ' || e.KeyChar == 8 || e.KeyChar >= '0' && e.KeyChar <= '9')
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void MaterialTextBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar >= '0' && e.KeyChar <= '9' || e.KeyChar == 8)
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void MaterialTextBox6_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar >= '0' && e.KeyChar <= '9' || e.KeyChar == 8)
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void MaterialTextBox3_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar >= '0' && e.KeyChar <= '9' || e.KeyChar == 8)
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void MaterialTextBox7_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar >= '0' && e.KeyChar <= '9' || e.KeyChar == 8)
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void MaterialTextBox8_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar >= '0' && e.KeyChar <= '9' || e.KeyChar == 8)
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void MaterialTextBox9_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar >= '0' && e.KeyChar <= '9' || e.KeyChar == 8)
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void MaterialTextBox4_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar >= '0' && e.KeyChar <= '9' || e.KeyChar == 8)
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void Panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void Addupdaassesscomponent_Load(object sender, EventArgs e)
        {
            try
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand sc = new SqlCommand("select Id from Rubric", con);
                SqlDataReader reader;
                reader = sc.ExecuteReader();
                DataTable dt = new DataTable();
                dt.Columns.Add("Id", typeof(string));
                dt.Load(reader);
                materialComboBox1.ValueMember = "Id";
                materialComboBox1.DataSource = dt;
            }
            catch (Exception)
            {

                throw;
            }
            try
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand sc = new SqlCommand("select Id from Rubric", con);
                SqlDataReader reader;
                reader = sc.ExecuteReader();
                DataTable dt = new DataTable();
                dt.Columns.Add("Id", typeof(string));
                dt.Load(reader);
                materialComboBox2.ValueMember = "Id";
                materialComboBox2.DataSource = dt;
            }
            catch (Exception)
            {

                throw;
            }
            try
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand sc = new SqlCommand("select Id from Assessment", con);
                SqlDataReader reader;
                reader = sc.ExecuteReader();
                DataTable dt = new DataTable();
                dt.Columns.Add("Id", typeof(string));
                dt.Load(reader);
                materialComboBox3.ValueMember = "Id";
                materialComboBox3.DataSource = dt;
            }
            catch (Exception)
            {

                throw;
            }
            try
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand sc = new SqlCommand("select Id from Assessment", con);
                SqlDataReader reader;
                reader = sc.ExecuteReader();
                DataTable dt = new DataTable();
                dt.Columns.Add("Id", typeof(string));
                dt.Load(reader);
                materialComboBox4.ValueMember = "Id";
                materialComboBox4.DataSource = dt;
            }
            catch (Exception)
            {

                throw;
            }
            try
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand sc = new SqlCommand("select Id from AssessmentComponent", con);
                SqlDataReader reader;
                reader = sc.ExecuteReader();
                DataTable dt = new DataTable();
                dt.Columns.Add("Id", typeof(string));
                dt.Load(reader);
                materialComboBox5.ValueMember = "Id";
                materialComboBox5.DataSource = dt;
            }
            catch (Exception)
            {

                throw;
            }
           
        }
    }
}
