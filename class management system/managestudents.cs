﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace class_management_system
{
    public partial class managestudents : Form
    {
        public managestudents()
        {
            InitializeComponent();
        }
        public void LoadGrid()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from Student", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            bunifuDataGridView1.DataSource = dt;
        }
        private void Managestudents_Load(object sender, EventArgs e)
        {

            LoadGrid();
            try
            {
               var con = Configuration.getInstance().getConnection();
                SqlCommand sc = new SqlCommand("select Id from Student", con);
                SqlDataReader reader;
                reader = sc.ExecuteReader();
                DataTable dt = new DataTable();
                dt.Columns.Add("Id", typeof(string));
                dt.Load(reader);
                materialComboBox5.ValueMember = "Id";
                materialComboBox5.DataSource = dt;
            }
            catch (Exception)
            {

                throw;
            }

        }

        private void Button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            Addstudent c = new Addstudent();
            c.Show();
        }

        private void Button2_Click(object sender, EventArgs e)
        {
         
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand(@"DELETE FROM ClassAttendance WHERE ClassAttendance.Id IN (SELECT ClassAttendance.Id FROM ClassAttendance JOIN Student ON ClassAttendance.Id = Student.Id WHERE Student.Id =@ID);
                                                  DELETE FROM StudentAttendance WHERE StudentAttendance.StudentId = @ID;
                                                  DELETE FROM StudentResult WHERE StudentResult.StudentId = @ID;
                                                  DELETE FROM AssessmentComponent WHERE AssessmentComponent.AssessmentId IN(SELECT Assessment.Id FROM Assessment JOIN Student ON Assessment.Id = Student.Id WHERE Student.Id = @ID);
                                                  DELETE FROM Lookup WHERE Lookup.LookupId IN(SELECT Lookup.LookupId FROM Lookup JOIN AssessmentComponent ON Lookup.LookupId = AssessmentComponent.Id JOIN Student ON AssessmentComponent.AssessmentId = Student.Id WHERE Student.Id = @ID);
                                                  DELETE FROM RubricLevel WHERE RubricLevel.RubricId IN(SELECT Rubric.Id FROM Rubric JOIN Clo ON Rubric.CloId = Clo.Id JOIN AssessmentComponent ON AssessmentComponent.RubricId = Rubric.Id JOIN Student ON AssessmentComponent.AssessmentId = Student.Id WHERE Student.Id = @ID);
                                                  DELETE FROM Rubric WHERE Rubric.CloId IN(SELECT Clo.Id FROM Clo JOIN AssessmentComponent ON Clo.Id = AssessmentComponent.Id JOIN Student ON AssessmentComponent.AssessmentId = Student.Id WHERE Student.Id = @ID);
                                                  DELETE FROM Clo WHERE Clo.Id IN(SELECT Clo.Id FROM Clo JOIN Rubric ON Clo.Id = Rubric.CloId JOIN AssessmentComponent ON AssessmentComponent.RubricId = Rubric.Id JOIN Student ON AssessmentComponent.AssessmentId = Student.Id WHERE Student.Id = @ID);
                                                  DELETE FROM Assessment WHERE Assessment.Id IN(SELECT Assessment.Id FROM Assessment JOIN Student ON Assessment.Id = Student.Id WHERE Student.Id = @ID);
                                                  DELETE FROM Student WHERE Student.Id = @ID;
                                                  ", con);
                cmd.Parameters.AddWithValue("@ID", int.Parse(materialComboBox5.Text));
                cmd.ExecuteNonQuery();
                MessageBox.Show("Successfully deleted...");
            
        }

        private void Button3_Click(object sender, EventArgs e)
        {
            this.Hide();
            updatestudent c = new updatestudent();
            c.Show();
        }

        private void BunifuDataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void MaterialTextBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar >= '0' && e.KeyChar <= '9' || e.KeyChar == 8)
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }
    }
    
}
