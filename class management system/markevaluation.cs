﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace class_management_system
{
    public partial class markevaluation : Form
    {
        public markevaluation()
        {
            InitializeComponent();
            //convertleveltoid();
            //convertcomponenttoid();
        }

        private void Button1_Click(object sender, EventArgs e)
        {

        }

        private void MaterialComboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void Panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        public int getMaxrubrilevel()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("SELECT Max(MeasurementLevel) FROM RubricLevel", con);

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);

            return (int)dt.Rows[0][0];
        }

        
        private void Markevaluation_Load(object sender, EventArgs e)
        {
      
            try
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand sc = new SqlCommand("select Id from Student", con);
                SqlDataReader reader;
                reader = sc.ExecuteReader();
                DataTable dt = new DataTable();
                dt.Columns.Add("Id", typeof(string));
                dt.Load(reader);
                materialComboBox1.ValueMember = "Id";
                materialComboBox1.DataSource = dt;
            }
            catch (Exception)
            {

                throw;
            }
            try
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand sc = new SqlCommand("select Title from Assessment", con);
                SqlDataReader reader;
                reader = sc.ExecuteReader();
                DataTable dt = new DataTable();
                dt.Columns.Add("Title", typeof(string));
                dt.Load(reader);
                materialComboBox2.ValueMember = "Title";
                materialComboBox2.DataSource = dt;
            }
            catch (Exception)
            {

                throw;
            }
            try
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand sc = new SqlCommand("select Id from RubricLevel", con);
                SqlDataReader reader;
                reader = sc.ExecuteReader();
                DataTable dt = new DataTable();
                dt.Columns.Add("Id", typeof(string));
                dt.Load(reader);
                materialComboBox4.ValueMember = "Id";
                materialComboBox4.DataSource = dt;
            }
            catch (Exception)
            {

                throw;
            }

        }

        private void MaterialComboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
           
        }

        private void Label1_Click(object sender, EventArgs e)
        {
            string name=materialComboBox2.Text;
            try
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand sc = new SqlCommand("SELECT AssessmentComponent.Id FROM Assessment JOIN AssessmentComponent ON Assessment.Id = AssessmentComponent.AssessmentId WHERE Assessment.Title = @Title", con);
                sc.Parameters.AddWithValue("@Title", name);
                SqlDataReader reader;
                reader = sc.ExecuteReader();
                DataTable dt = new DataTable();
                dt.Columns.Add("Id", typeof(string));
                dt.Load(reader);
                materialComboBox3.ValueMember = "Id";
                materialComboBox3.DataSource = dt;
            }
            catch (Exception)
            {

                throw;
            }

        }

        private void MaterialTextBox1_TextChanged(object sender, EventArgs e)
        {
           
        }

        private void Button1_Click_1(object sender, EventArgs e)
        {
          //  string stdid = convertcomponenttoid();
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Insert into StudentResult values (@StudentId, @AssessmentComponentId,@RubricMeasurementId,@EvaluationDate)", con);
            cmd.Parameters.AddWithValue("@StudentId", int.Parse(materialComboBox1.Text));
            cmd.Parameters.AddWithValue("@AssessmentComponentId",int.Parse(materialComboBox3.Text));
            cmd.Parameters.AddWithValue("@RubricMeasurementId",int.Parse(materialComboBox4.Text));
            cmd.Parameters.AddWithValue("@EvaluationDate", System.DateTime.Now);
            cmd.ExecuteNonQuery();
            MessageBox.Show("Successfully saved...");
        }

     /*   public void convertleveltoid()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from RubricLevel", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);


            materialComboBox4.DataSource = dt;
            materialComboBox4.DisplayMember = "Details";
            materialComboBox4.ValueMember = "Id";
            materialComboBox4.SelectedIndex = 0;

        }
        public void convertcomponenttoid()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from AssessmentComponent", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);


            materialComboBox3.DataSource = dt;
            materialComboBox3.DisplayMember = "Name";
            materialComboBox3.ValueMember = "Id";
            materialComboBox3.SelectedIndex = 0;

        }*/

        private void MaterialComboBox4_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void Button3_Click(object sender, EventArgs e)
        {

        }
    }
}
