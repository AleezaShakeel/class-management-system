﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;


namespace class_management_system
{
    public partial class updatestudent : Form
    {
        public updatestudent()
        {
            InitializeComponent();
        }

        private void PictureBox1_Click(object sender, EventArgs e)
        {
            this.Hide();
            dashboard f = new dashboard();
            f.Show();
        }
        bool IsValidEmail(string eMail)
        {
            bool Result = true;

            try
            {
                var eMailValidator = new System.Net.Mail.MailAddress(eMail);

                Result = (eMail.LastIndexOf(".") > eMail.LastIndexOf("@"));
            }
            catch
            {
                Result = false;
            };

            return Result;
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            if (materialTextBox1.Text == "")
            {
                label3.Text = "Please enter First Name";
            }
            if (materialTextBox2.Text == "")
            {
                label4.Text = "Please enter Last Name";
            }
            if (materialTextBox3.Text == "")
            {
                label5.Text = "Please enter Contact";
            }
            if (materialTextBox4.Text == "")
            {
                label6.Text = "Please enter Email";
            }
            if (materialTextBox5.Text == "")
            {
                label7.Text = "Please enter Registration Number";
            }
            else
            {  
                label3.Text = " ";
                label4.Text = " ";
                label5.Text = " ";
                label6.Text = " ";
                label7.Text = " ";
                string contact = materialTextBox3.Text;
                string email = materialTextBox4.Text;
                string regno = materialTextBox5.Text;
                if (materialTextBox1.Text.Length > 1)
                {
                    if (materialTextBox2.Text.Length > 1)
                    {
                        if (contact.Length == 11 && contact[0] == '0' && contact[1] == '3')
                        {
                            if (IsValidEmail(email))
                            {
                                if (regno[4] == '-' && regno[7] == '-')
                                {
                                    int sta = 0;
                                    if (materialComboBox1.Text == "Active")
                                    {
                                        sta = 5;
                                    }
                                    else
                                    {
                                        sta = 6;
                                    }
                                    var con = Configuration.getInstance().getConnection();
                                    SqlCommand cmd = new SqlCommand("UPDATE Student SET FirstName=@FirstName, LastName=@LastName,Contact=@Contact,Email=@Email,RegistrationNumber=@RegistrationNumber,Status=@Status WHERE ID=@ID", con);
                                    con.Close();
                                    con.Open();
                                    cmd.Parameters.AddWithValue("@ID", int.Parse(materialComboBox5.Text));
                                    cmd.Parameters.AddWithValue("@FirstName", materialTextBox1.Text);
                                    cmd.Parameters.AddWithValue("@LastName", materialTextBox2.Text);
                                    cmd.Parameters.AddWithValue("@Contact", materialTextBox3.Text);
                                    cmd.Parameters.AddWithValue("@Email", materialTextBox4.Text);
                                    cmd.Parameters.AddWithValue("@RegistrationNumber", materialTextBox5.Text);
                                    cmd.Parameters.AddWithValue("@Status", sta);
                                    cmd.ExecuteNonQuery();
                                    MessageBox.Show("Successfully Updated...");
                                }
                                else
                                {
                                    MessageBox.Show("Invalid Registeration Number...");
                                }
                            }
                            else
                            {
                                MessageBox.Show("Invali Email...");
                            }
                        }
                        else
                        {
                            MessageBox.Show("Invalid Contact...");
                        }
                    }
                    else
                    {
                        MessageBox.Show("Invalid Last Name...");
                    }
                }
                else
                {
                    MessageBox.Show("Invalid First Name...");
                }

               
            }
        }

        private void MaterialTextBox6_TextChanged(object sender, EventArgs e)
        {

        
        }

        private void MaterialTextBox5_TextChanged(object sender, EventArgs e)
        {

        }

        private void MaterialTextBox4_TextChanged(object sender, EventArgs e)
        {

        }

        private void MaterialTextBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void MaterialTextBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void Label1_Click(object sender, EventArgs e)
        {

        }

        private void MaterialTextBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void Updatestudent_Load(object sender, EventArgs e)
        {

            try
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand sc = new SqlCommand("select Id from Student", con);
                SqlDataReader reader;
                reader = sc.ExecuteReader();
                DataTable dt = new DataTable();
                dt.Columns.Add("Id", typeof(string));
                dt.Load(reader);
                materialComboBox5.ValueMember = "Id";
                materialComboBox5.DataSource = dt;
            }
            catch (Exception)
            {

                throw;
            }
            //var con = Configuration.getInstance().getConnection();
            //SqlCommand cmd = new SqlCommand("Select * from Student", con);
            //SqlDataAdapter da = new SqlDataAdapter(cmd);
            ///DataTable dt = new DataTable();
            //da.Fill(dt);
            //bunifuDataGridView1.DataSource = dt;
        }

        private void MaterialTextBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar >= 'a' && e.KeyChar <= 'z' || e.KeyChar >= 'A' && e.KeyChar <= 'Z' || e.KeyChar == ' ' || e.KeyChar == 8)
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void MaterialTextBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar >= 'a' && e.KeyChar <= 'z' || e.KeyChar >= 'A' && e.KeyChar <= 'Z' || e.KeyChar == ' ' || e.KeyChar == 8)
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void MaterialTextBox3_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar >= '0' && e.KeyChar <= '9' || e.KeyChar == 8)
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void MaterialTextBox4_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar >= '0' && e.KeyChar <= '9' || e.KeyChar == 8 || e.KeyChar >= 'a' && e.KeyChar <= 'z' || e.KeyChar >= 'A' && e.KeyChar <= 'Z' || e.KeyChar == '@' || e.KeyChar == '.')
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void MaterialTextBox5_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar >= '0' && e.KeyChar <= '9' || e.KeyChar == 8 || e.KeyChar == 'C' || e.KeyChar == 'S' || e.KeyChar == '-')
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void MaterialTextBox6_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar >= '5' && e.KeyChar <= '6' || e.KeyChar == 8)
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void Button2_Click(object sender, EventArgs e)
        {

                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("Select * from Student where ID=@ID", con);
                cmd.Parameters.AddWithValue("@ID", int.Parse(materialComboBox5.Text));
                SqlDataReader da = cmd.ExecuteReader();
                while (da.Read())
                {
                    materialTextBox1.Text = da.GetValue(1).ToString();
                    materialTextBox2.Text = da.GetValue(2).ToString();
                    materialTextBox3.Text = da.GetValue(3).ToString();
                    materialTextBox4.Text = da.GetValue(4).ToString();
                    materialTextBox5.Text = da.GetValue(5).ToString();
                    //materialTextBox6.Text = da.GetValue(6).ToString();
                }
            da.Close();
            
            
        }

        private void MaterialTextBox7_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar >= '0' && e.KeyChar <= '9' || e.KeyChar == 8)
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void Label3_Click(object sender, EventArgs e)
        {

        }

        private void Label6_Click(object sender, EventArgs e)
        {

        }
    }
}
