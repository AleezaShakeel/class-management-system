﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace class_management_system
{
    public partial class manageattendance : Form
    {
        public manageattendance()
        {
            InitializeComponent();
        }
        

        private void Manageattendance_Load(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select Id, FirstName, LastName from Student", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            bunifuDataGridView1.DataSource = dt;

        }
     

        private void BunifuDataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void Button2_Click(object sender, EventArgs e)
        {
            DateTime dateTime = bunifuDatePicker1.Value;
            var con = Configuration.getInstance().getConnection();

            SqlCommand cmd = new SqlCommand("SELECT Id from [ClassAttendance] Where YEAR(AttendanceDate) = @YEAR AND MONTH(AttendanceDate) = @MONTH AND DAY(AttendanceDate) = @DAY ", con);
            cmd.Parameters.AddWithValue("@YEAR", dateTime.Year);
            cmd.Parameters.AddWithValue("@MONTH", dateTime.Month);
            cmd.Parameters.AddWithValue("@DAY", dateTime.Day);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable checkdt = new DataTable();
            da.Fill(checkdt);

            if (checkdt.Rows.Count > 0)
            {
                cmd = new SqlCommand("DELETE from StudentAttendance Where AttendanceId = @id", con);
                cmd.Parameters.AddWithValue("@id", (int)checkdt.Rows[0][0]);
                cmd.ExecuteNonQuery();
            }
            else
            {
                cmd = new SqlCommand("Insert into [ClassAttendance] values (@attendancedate)", con);
                cmd.Parameters.AddWithValue("@attendancedate", dateTime);
                cmd.ExecuteNonQuery();
            }

            cmd = new SqlCommand("SELECT Id from [ClassAttendance] Where YEAR(AttendanceDate) = @YEAR AND MONTH(AttendanceDate) = @MONTH AND DAY(AttendanceDate) = @DAY ", con);
            cmd.Parameters.AddWithValue("@YEAR", dateTime.Year);
            cmd.Parameters.AddWithValue("@MONTH", dateTime.Month);
            cmd.Parameters.AddWithValue("@DAY", dateTime.Day);
            SqlDataAdapter da1 = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da1.Fill(dt);

            int attendaceid = (int)dt.Rows[0][0];


            for (int i = 1; i < bunifuDataGridView1.Rows.Count; i++)
            {
                DataGridViewRow row = bunifuDataGridView1.Rows[i];
                cmd = new SqlCommand("Insert into StudentAttendance values (@AttenadnecId , @StudentId,@Attendancestaus)", con);
                cmd.Parameters.AddWithValue("@AttenadnecId", attendaceid);
                cmd.Parameters.AddWithValue("@StudentId", (int)row.Cells[1].Value);

                if (row.Cells[0].Value.ToString() == "Present")
                {
                    cmd.Parameters.AddWithValue("@Attendancestaus", 1);
                }
                else if (row.Cells[0].Value.ToString() == "Absent")
                {
                    cmd.Parameters.AddWithValue("@Attendancestaus", 2);
                }
                else if (row.Cells[0].Value.ToString() == "Leave")
                {
                    cmd.Parameters.AddWithValue("@Attendancestaus", 3);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@Attendancestaus", 4);
                }


                cmd.ExecuteNonQuery();
                MessageBox.Show("Attendance Saved Successfully...");
            }
        }
        public void AttendancebindData()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select Id,Concat(FirstName,LastName) As StudentName,RegistrationNumber From Student ", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);

            bunifuDataGridView1.DataSource = dt;
        }
    }
    
}
