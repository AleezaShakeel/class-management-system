﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;  
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace class_management_system
{
    public partial class add_and_update_assessment : Form
    {
        public add_and_update_assessment()
        {
            InitializeComponent();
        }

        private void PictureBox1_Click(object sender, EventArgs e)
        {
            this.Hide();
            dashboard c = new dashboard();
            c.Show();
        }

        private void Label3_Click(object sender, EventArgs e)
        {

        }

        private void BunifuThinButton22_Click(object sender, EventArgs e)
        {
            if (materialTextBox1.Text == "")
            {
                label4.Text = "Please enter Assessment Title";
            }
            if (materialTextBox2.Text == "")
            {
                label5.Text = "Please enter Total Marks";
            }
            if (materialTextBox3.Text == "")
            {
                label6.Text = "Please enter Total Weightage";
            }
            else
            {
                label4.Text = " ";
                label5.Text = " ";
                label6.Text = " ";
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("Insert into Assessment values (@Title, @DateCreated,@TotalMarks,@TotalWeightage)", con);
                cmd.Parameters.AddWithValue("@Title", materialTextBox1.Text);
                cmd.Parameters.AddWithValue("@DateCreated", System.DateTime.Now);
                cmd.Parameters.AddWithValue("@TotalMarks", int.Parse(materialTextBox2.Text));
                cmd.Parameters.AddWithValue("@TotalWeightage",int.Parse(materialTextBox3.Text));
                cmd.ExecuteNonQuery();
                MessageBox.Show("Successfully saved...");
            }
        }

        private void BunifuThinButton24_Click(object sender, EventArgs e)
        {
            if (materialTextBox5.Text == "")
            {
                label8.Text = "Please enter Assessment Title";
            }
            if (materialTextBox6.Text == "")
            {
                label9.Text = "Please enter Total Marks";
            }
            if (materialTextBox7.Text == "")
            {
                label10.Text = "Please enter Total Weightage";
            }
            else
            {
                label8.Text = " ";
                label9.Text = " ";
                label10.Text = " ";
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("UPDATE Assessment SET Title=@Title, TotalMarks=@TotalMarks,TotalWeightage=@TotalWeightage WHERE Id=@Id", con);
                con.Close();
                con.Open();
                cmd.Parameters.AddWithValue("@Id", int.Parse(materialTextBox4.Text));
                cmd.Parameters.AddWithValue("@Title", materialTextBox5.Text);
                cmd.Parameters.AddWithValue("@TotalMarks", int.Parse(materialTextBox6.Text));
                cmd.Parameters.AddWithValue("@TotalWeightage",int.Parse(materialTextBox7.Text));
                cmd.ExecuteNonQuery();
                MessageBox.Show("Successfully Updated...");
            }
        }

        private void BunifuThinButton21_Click(object sender, EventArgs e)
        {
            if (materialTextBox4.Text == "")
            {
                label7.Text = "Please enter ID to Load data";
            }
            else
            {
                label7.Text = " ";
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("Select * from Assessment where Id=@Id", con);
                cmd.Parameters.AddWithValue("@Id", int.Parse(materialTextBox4.Text));
                SqlDataReader da = cmd.ExecuteReader();
                while (da.Read())
                {
                    materialTextBox5.Text = da.GetValue(1).ToString();
                    materialTextBox6.Text = da.GetValue(3).ToString();
                    materialTextBox7.Text = da.GetValue(4).ToString();
                    
                }
            }
           
        }

        private void Label4_Click(object sender, EventArgs e)
        {

        }

        private void MaterialTextBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar >= 'a' && e.KeyChar <= 'z' || e.KeyChar >= 'A' && e.KeyChar <= 'Z' || e.KeyChar == ' ' || e.KeyChar == 8|| e.KeyChar >= '0' && e.KeyChar <= '9')
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void MaterialTextBox5_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar >= 'a' && e.KeyChar <= 'z' || e.KeyChar >= 'A' && e.KeyChar <= 'Z' || e.KeyChar == ' ' || e.KeyChar == 8 || e.KeyChar >= '0' && e.KeyChar <= '9')
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void MaterialTextBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar >= '0' && e.KeyChar <= '9' || e.KeyChar == 8)
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void MaterialTextBox6_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar >= '0' && e.KeyChar <= '9' || e.KeyChar == 8)
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void MaterialTextBox3_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar >= '0' && e.KeyChar <= '9' || e.KeyChar == 8)
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void MaterialTextBox7_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar >= '0' && e.KeyChar <= '9' || e.KeyChar == 8)
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void MaterialTextBox4_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar >= '0' && e.KeyChar <= '9' || e.KeyChar == 8)
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void MaterialTextBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void Add_and_update_assessment_Load(object sender, EventArgs e)
        {

        }
    }
}
