﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace class_management_system
{
    public partial class updateclo : Form
    {
        public updateclo()
        {
            InitializeComponent();
        }

        private void Label1_Click(object sender, EventArgs e)
        {

        }

        private void Label1_Click_1(object sender, EventArgs e)
        {

        }

        private void PictureBox1_Click(object sender, EventArgs e)
        {
            this.Hide();
            dashboard f = new dashboard();
            f.Show();
        }

        private void MaterialComboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void Updateclo_Load(object sender, EventArgs e)
        {
            try
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand sc = new SqlCommand("select Name from Clo", con);
                SqlDataReader reader;
                reader = sc.ExecuteReader();
                DataTable dt = new DataTable();
                dt.Columns.Add("Name", typeof(string));
                dt.Load(reader);
                materialComboBox1.ValueMember = "Name";
                materialComboBox1.DataSource = dt;
            }
            catch (Exception)
            {

                throw;
            }
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            if (materialTextBox1.Text == "")
            {
                label2.Text = "Enter Clo Name To Update";
            }
            else
            {
                label2.Text = " ";
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("UPDATE Clo SET Name=@NewName WHERE Name=@OldName", con);
                cmd.Parameters.AddWithValue("@NewName", materialTextBox1.Text);
                cmd.Parameters.AddWithValue("@OldName", materialComboBox1.Text);
                cmd.ExecuteNonQuery();
                MessageBox.Show("Successfully Updated...");
            }
        }

        private void MaterialTextBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 'C' || e.KeyChar >= 'l' || e.KeyChar <= 'o' || e.KeyChar >= '0' && e.KeyChar <= '9' || e.KeyChar == 8)
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void Panel1_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
