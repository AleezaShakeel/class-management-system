﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace class_management_system
{
    public partial class manageassessmentcomponents : Form
    {
        public manageassessmentcomponents()
        {
            InitializeComponent();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            addupdaassesscomponent c = new addupdaassesscomponent();
            c.Show();
        }

        private void PictureBox2_Click(object sender, EventArgs e)
        {
            this.Hide();
            dashboard f = new dashboard();
            f.Show();
        }

        private void Button3_Click(object sender, EventArgs e)
        {
            this.Hide();
            addupdaassesscomponent c = new addupdaassesscomponent();
            c.Show();
        }
        public void LoadGrid()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from AssessmentComponent", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            bunifuDataGridView1.DataSource = dt;
        }

        private void Manageassessmentcomponents_Load(object sender, EventArgs e)
        {
            LoadGrid();
            try
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand sc = new SqlCommand("select Id from AssessmentComponent", con);
                SqlDataReader reader;
                reader = sc.ExecuteReader();
                DataTable dt = new DataTable();
                dt.Columns.Add("Id", typeof(string));
                dt.Load(reader);
                materialComboBox5.ValueMember = "Id";
                materialComboBox5.DataSource = dt;
            }
            catch (Exception)
            {

                throw;
            }
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("DELETE FROM AssessmentComponent WHERE Id=@Id", con);
            cmd.Parameters.AddWithValue("@Id", int.Parse(materialComboBox5.Text));
            cmd.ExecuteNonQuery();
            MessageBox.Show("Successfully deleted...");
        }

        private void Panel1_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
