﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace class_management_system
{
    public partial class manageassessment : Form
    {
        public manageassessment()
        {
            InitializeComponent();
        }

        public void LoadGrid()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from Assessment", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            bunifuDataGridView1.DataSource = dt;
        }
        private void Manageassessment_Load(object sender, EventArgs e)
        {
            LoadGrid();
            try
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand sc = new SqlCommand("select Id from Assessment", con);
                SqlDataReader reader;
                reader = sc.ExecuteReader();
                DataTable dt = new DataTable();
                dt.Columns.Add("Id", typeof(string));
                dt.Load(reader);
                materialComboBox5.ValueMember = "Id";
                materialComboBox5.DataSource = dt;
            }
            catch (Exception)
            {

                throw;
            }
        }

        private void Panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void BunifuThinButton24_Click(object sender, EventArgs e)
        {

        }

        private void MaterialTextBox7_TextChanged(object sender, EventArgs e)
        {

        }

        private void Label1_Click(object sender, EventArgs e)
        {

        }

        private void TableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void BunifuThinButton22_Click(object sender, EventArgs e)
        {
        }

        private void BunifuThinButton24_Click_1(object sender, EventArgs e)
        {
        }

        private void Button2_Click(object sender, EventArgs e)
        {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("DELETE FROM AssessmentComponent WHERE AssessmentId = @Id; DELETE FROM Assessment WHERE Id = @Id;", con);
                cmd.Parameters.AddWithValue("@Id", int.Parse(materialComboBox5.Text));
         
                cmd.ExecuteNonQuery();
                MessageBox.Show("Successfully deleted...");
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            add_and_update_assessment c = new add_and_update_assessment();
            c.Show();
        }

        private void Button3_Click(object sender, EventArgs e)
        {
            this.Hide();
            add_and_update_assessment c = new add_and_update_assessment();
            c.Show();
        }

        private void Button4_Click(object sender, EventArgs e)
        {
            this.Hide();
            manageassessmentcomponents c = new manageassessmentcomponents();
            c.Show();
        }

        private void MaterialTextBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar >= '0' && e.KeyChar <= '9' || e.KeyChar == 8)
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }
    }
}
