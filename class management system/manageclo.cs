﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace class_management_system
{
    public partial class manageclo : Form
    {
        public manageclo()
        {
            InitializeComponent();
        }

        private void Label1_Click(object sender, EventArgs e)
        {

        }
        public int getMaxrubricid()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("SELECT Max(Id) FROM Rubric", con);

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);

            return (int)dt.Rows[0][0];
        }

        
        private void BunifuThinButton21_Click(object sender, EventArgs e)
        {
            if (materialTextBox2.Text == "")
            {
                label7.Text = "Please enter Rubric name";
            }
            if (materialTextBox5.Text == "")
            {
                label11.Text = "Please enter CLO Id";
            }
            else
            {

                label7.Text = " ";
                label11.Text = " ";
                int id = getMaxrubricid();
                id++;
                var con = Configuration.getInstance().getConnection();
                con.Close();
                con.Open();
                SqlCommand selectCommand = new SqlCommand("SELECT Id FROM Clo WHERE Id = @CloId ", con);
                selectCommand.Parameters.AddWithValue("@cloId", int.Parse(materialTextBox5.Text));
                SqlDataReader reader = selectCommand.ExecuteReader();
                if (!reader.HasRows)
                {
                    //Console.WriteLine("RubricId not found in Rubric table");
                    MessageBox.Show("CloId not found in Clo table");
                    return; // exit the method or handle the error as appropriate
                }
                reader.Close();
                SqlCommand insertCommand = new SqlCommand("INSERT INTO Rubric (Id, Details, CloId) VALUES (@Id, @Details, @Cloid)", con);

                // SqlCommand cmd = new SqlCommand("Insert into Rubric values (@Id,@Details,@CloId)", con);
               // insertCommand.Parameters.AddWithValue("@RubricId", int.Parse(materialTextBox3.Text));
                //insertCommand.Parameters.AddWithValue("@Details", materialTextBox6.Text);
                //insertCommand.Parameters.AddWithValue("@MeasurementLevel", int.Parse(materialTextBox4.Text));
                insertCommand.Parameters.AddWithValue("@id", id);
                insertCommand.Parameters.AddWithValue("@Details", materialTextBox2.Text);
                insertCommand.Parameters.AddWithValue("@cloId", int.Parse(materialTextBox5.Text));
                int rowsAffected = insertCommand.ExecuteNonQuery();
                MessageBox.Show("Successfully saved...");

                // create a SqlConnection object and open the connection
                // using (SqlConnection connection = new SqlConnection(connectionString))
                // {
                // var con = Configuration.getInstance().getConnection();



                // create a SqlCommand object for the SELECT statement to check if the RubricId exists
                // SqlCommand selectCommand = new SqlCommand("SELECT Id FROM Rubric WHERE Id = @RubricId", con);
                //selectCommand.Parameters.AddWithValue("@RubricId", int.Parse(materialTextBox3.Text));

                // execute the SELECT statement and check if the RubricId exists
                //  SqlDataReader reader = selectCommand.ExecuteReader();
                //  if (!reader.HasRows)
                // {
                //Console.WriteLine("RubricId not found in Rubric table");
                //    MessageBox.Show("RubricId not found in Rubric table");
                //   return; // exit the method or handle the error as appropriate
                //}
                // reader.Close();

                // create a SqlCommand object for the INSERT statement to insert the record into RubricLevel
                //SqlCommand insertCommand = new SqlCommand("INSERT INTO RubricLevel (RubricId, Details, MeasurementLevel) VALUES (@RubricId, @Details, @MeasurementLevel)", con);
                //insertCommand.Parameters.AddWithValue("@RubricId", int.Parse(materialTextBox3.Text));
                // insertCommand.Parameters.AddWithValue("@Details", materialTextBox6.Text);
                // insertCommand.Parameters.AddWithValue("@MeasurementLevel", int.Parse(materialTextBox4.Text));

                // execute the INSERT statement to insert the record into RubricLevel
                // int rowsAffected = insertCommand.ExecuteNonQuery();

            }

        }
    

        private void BunifuThinButton22_Click(object sender, EventArgs e)
        {
            if(materialTextBox1.Text=="")
            {
                label6.Text = "Please enter CLO name";
            }
            else
            {

                label6.Text = " ";
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("Insert into Clo values (@Name,@DateCreated,@DateUpdated)", con);
                cmd.Parameters.AddWithValue("@Name", materialTextBox1.Text);
                cmd.Parameters.AddWithValue("@DateCreated",System.DateTime.Today);
                cmd.Parameters.AddWithValue("@DateUpdated", System.DateTime.Today);
                cmd.ExecuteNonQuery();
                MessageBox.Show("Successfully saved...");
            }
        }

        private void BunifuThinButton23_Click(object sender, EventArgs e)
        {
            if (materialTextBox3.Text == "")
            {
                label12.Text = "Please enter Rubric Id";
            }
            if (materialTextBox6.Text == "")
            {
                label9.Text = "Please enter Rubric Level Name";
            }
            if (materialTextBox4.Text == "")
            {
                label10.Text = "Please enter Rubric Measurement";
            }

            else
            {

                label12.Text = " ";
                label9.Text = " ";
                label10.Text = " ";
                // create a SqlConnection object and open the connection
                // using (SqlConnection connection = new SqlConnection(connectionString))
                // {
                var con = Configuration.getInstance().getConnection();
                con.Close();
                    con.Open();

                    
                    // create a SqlCommand object for the SELECT statement to check if the RubricId exists
                    SqlCommand selectCommand = new SqlCommand("SELECT Id FROM Rubric WHERE Id = @RubricId", con);
                    selectCommand.Parameters.AddWithValue("@RubricId", int.Parse(materialTextBox3.Text));

                    // execute the SELECT statement and check if the RubricId exists
                    SqlDataReader reader = selectCommand.ExecuteReader();
                    if (!reader.HasRows)
                    {
                    //Console.WriteLine("RubricId not found in Rubric table");
                    MessageBox.Show("RubricId not found in Rubric table");
                        return; // exit the method or handle the error as appropriate
                    }
                    reader.Close();

                    // create a SqlCommand object for the INSERT statement to insert the record into RubricLevel
                    SqlCommand insertCommand = new SqlCommand("INSERT INTO RubricLevel (RubricId, Details, MeasurementLevel) VALUES (@RubricId, @Details, @MeasurementLevel)", con);
                    insertCommand.Parameters.AddWithValue("@RubricId", int.Parse(materialTextBox3.Text));
                    insertCommand.Parameters.AddWithValue("@Details", materialTextBox6.Text);
                    insertCommand.Parameters.AddWithValue("@MeasurementLevel",int.Parse(materialTextBox4.Text));

                    // execute the INSERT statement to insert the record into RubricLevel
                    int rowsAffected = insertCommand.ExecuteNonQuery();
                MessageBox.Show("Successfully saved...");


                // }

            }
        }
       

        private void BunifuThinButton24_Click(object sender, EventArgs e)
        {
            this.Hide();
            updateclo f = new updateclo();
            f.Show();
        }

        private void BunifuThinButton25_Click(object sender, EventArgs e)
        {
            
        }

        private void BunifuThinButton26_Click(object sender, EventArgs e)
        {
           
        }

        private void Panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void MaterialTextBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void MaterialTextBox5_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar >= '0' && e.KeyChar <= '9' || e.KeyChar == 8)
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void MaterialTextBox3_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar >= '0' && e.KeyChar <= '9' || e.KeyChar == 8)
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void MaterialTextBox4_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar >= '0' && e.KeyChar <= '9' || e.KeyChar == 8)
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void MaterialTextBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 'C'|| e.KeyChar >= 'l' || e.KeyChar <= 'o' || e.KeyChar >= '0' && e.KeyChar <= '9' || e.KeyChar == 8)
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void MaterialTextBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar >= 'a' && e.KeyChar <= 'z' || e.KeyChar >= 'A' && e.KeyChar <= 'Z' || e.KeyChar == ' ' || e.KeyChar == 8)
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void MaterialTextBox6_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar >= 'a' && e.KeyChar <= 'z' || e.KeyChar >= 'A' && e.KeyChar <= 'Z' || e.KeyChar == ' ' || e.KeyChar == 8)
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void Manageclo_Load(object sender, EventArgs e)
        {

        }

        private void Label3_Click(object sender, EventArgs e)
        {

        }
    }
}
