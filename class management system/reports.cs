﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;


namespace class_management_system
{
    public partial class reports : Form
    {
        public reports()
        {
            InitializeComponent();
        }

        private void MaterialComboBox5_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void Label1_Click(object sender, EventArgs e)
        {
            if (materialComboBox5.Text == "CLO wise class result")
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand(@"SELECT s.Id AS StudentId, s.FirstName, s.LastName, c.Name AS CLOName, a.Title AS AssessmentTitle, ac.Name AS ComponentName, ac.TotalMarks AS ComponentTotalMarks, 
                                                rl.MeasurementLevel AS ObtainedRubricLevel, rl.RubricId, r.Details AS RubricDetails, r.CloId, a.TotalWeightage,
                                                (rl.MeasurementLevel / MAX(rl.MeasurementLevel) OVER(PARTITION BY ac.Id)) * ac.TotalMarks AS ObtainedMarks
                                                FROM Student s
                                                INNER JOIN StudentResult sr ON s.Id = sr.StudentId
                                                INNER JOIN AssessmentComponent ac ON sr.AssessmentComponentId = ac.Id
                                                INNER JOIN RubricLevel rl ON sr.RubricMeasurementId = rl.Id
                                                INNER JOIN Rubric r ON rl.RubricId = r.Id
                                                INNER JOIN Clo c ON r.CloId = c.Id
                                                INNER JOIN Assessment a ON ac.AssessmentId = a.Id
                                                WHERE s.Status = 5
                                                ORDER BY s.Id, c.Id, a.Id, ac.Id, r.Id, rl.Id", con);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                bunifuDataGridView1.DataSource = dt;
            }
            else if (materialComboBox5.Text == "Assessment wise class result")
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand(
                                                @"SELECT StudentResult.StudentId, RubricLevel.MeasurementLevel, (RubricLevel.MeasurementLevel / RubricLevelMax.MaxMeasurementLevel) * AssessmentComponent.TotalMarks AS ObtainedMarks, RubricLevelMax.MaxMeasurementLevel, AssessmentComponent.TotalMarks
                                                FROM StudentResult
                                                INNER JOIN RubricLevel ON StudentResult.RubricMeasurementId = RubricLevel.Id
                                                INNER JOIN AssessmentComponent ON StudentResult.AssessmentComponentId = AssessmentComponent.Id
                                                CROSS JOIN(SELECT MAX(MeasurementLevel) AS MaxMeasurementLevel FROM RubricLevel) RubricLevelMax
                                                GROUP BY StudentResult.StudentId, RubricLevel.MeasurementLevel, RubricLevelMax.MaxMeasurementLevel, AssessmentComponent.TotalMarks;
                                                ", con);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                bunifuDataGridView1.DataSource = dt;
            }
            else if(materialComboBox5.Text== "Attendance Report")
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("SELECT A.AttendanceDate,  COUNT(CASE WHEN SA.AttendanceStatus = 1 THEN S.Id END) AS TotalPresent,   COUNT(CASE WHEN SA.AttendanceStatus = 2 THEN S.Id END) AS TotalAbsent ,  COUNT(CASE WHEN SA.AttendanceStatus = 3 THEN S.Id END) AS LeaveStudent ,  COUNT(CASE WHEN SA.AttendanceStatus = 4 THEN S.Id END) AS LateStudent FROM ClassAttendance A LEFT JOIN StudentAttendance SA ON A.Id = SA.AttendanceId \r\nLEFT JOIN Student S ON SA.StudentId = S.Id  GROUP BY A.AttendanceDate", con);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                bunifuDataGridView1.DataSource = dt;
            }
        }
        public void GeneratePDF(DataGridView grid)
        {
            //Create a new PDF document
            Document document = new Document();
            PdfWriter.GetInstance(document, new FileStream("output.pdf", FileMode.Create));

            //Open the document for writing
            document.Open();

            //Create a new table with the same number of columns as the grid
            PdfPTable pdfTable = new PdfPTable(grid.Columns.Count);

            //Add the column headers to the table
            for (int i = 0; i < grid.Columns.Count; i++)
            {
                pdfTable.AddCell(new Phrase(grid.Columns[i].HeaderText));
            }

            //Add the rows to the table
            for (int i = 0; i < grid.Rows.Count; i++)
            {
                for (int j = 0; j < grid.Columns.Count; j++)
                {
                    //Check if the cell value is not null before adding it to the table
                    if (grid.Rows[i].Cells[j].Value != null)
                    {
                        pdfTable.AddCell(new Phrase(grid.Rows[i].Cells[j].Value.ToString()));
                    }
                    else
                    {
                        pdfTable.AddCell(new Phrase(""));
                    }
                }
            }

            //Add the table to the document
            document.Add(pdfTable);

            //Close the document
            document.Close();
            MessageBox.Show("Report Created Sucessfully...");
        }


        private void Reports_Load(object sender, EventArgs e)
        {

        }

        private void Button2_Click(object sender, EventArgs e)
        {
             GeneratePDF(bunifuDataGridView1);
        }
    }
}
