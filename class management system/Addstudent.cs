﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace class_management_system
{
    public partial class Addstudent : Form
    {
        public Addstudent()
        {
            InitializeComponent();
        }

        private void BunifuLabel1_Click(object sender, EventArgs e)
        {

        }

        private void MaterialTextBox1_TextChanged(object sender, EventArgs e)
        {

        }
        bool IsValidEmail(string eMail)
        {
            bool Result = true;

            try
            {
                var eMailValidator = new System.Net.Mail.MailAddress(eMail);

                Result = (eMail.LastIndexOf(".") > eMail.LastIndexOf("@"));
            }
            catch
            {
                Result = false;
            };

            return Result;
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            if (materialTextBox1.Text == "")
            {
                label2.Text = "Please enter First Name";
            }
            else { 
}
            if (materialTextBox2.Text == "")
            {
                label3.Text = "Please enter Last Name";
            }
            if (materialTextBox3.Text == "")
            {
                label4.Text = "Please enter Contact";
            }
            if (materialTextBox4.Text == "")
            {
                label5.Text = "Please enter Email";
            }
            if (materialTextBox5.Text == "")
            {
                label6.Text = "Please enter Registration Number";
            }
            else
            {
                label2.Text = " ";
                label3.Text = " ";
                label4.Text = " ";
                label5.Text = " ";
                label6.Text = " ";
                string contact = materialTextBox3.Text;
                string email = materialTextBox4.Text;
                string regno = materialTextBox5.Text;
                if (materialTextBox1.Text.Length>1)
                {
                    if (materialTextBox2.Text.Length > 1)
                    {
                        if(contact.Length==11 && contact[0]=='0' && contact[1] == '3')
                        {
                            if (IsValidEmail(email))
                            {
                                if (regno[4] == '-' && regno[7] == '-')
                                {
                                    int sta = 0;
                                    if (materialComboBox1.Text == "Active")
                                    {
                                        sta = 5;
                                    }
                                    else
                                    {
                                        sta = 6;
                                    }
                                    var con = Configuration.getInstance().getConnection();
                                    SqlCommand cmd = new SqlCommand("Insert into Student values (@FirstName, @LastName,@Contact,@Email,@RegisterationNumber,@Status)", con);
                                    cmd.Parameters.AddWithValue("@FirstName", materialTextBox1.Text);
                                    cmd.Parameters.AddWithValue("@LastName", materialTextBox2.Text);
                                    cmd.Parameters.AddWithValue("@Contact", materialTextBox3.Text);
                                    cmd.Parameters.AddWithValue("@Email", materialTextBox4.Text);
                                    cmd.Parameters.AddWithValue("@RegisterationNumber", materialTextBox5.Text);
                                    cmd.Parameters.AddWithValue("@Status", sta);
                                    cmd.ExecuteNonQuery();
                                    MessageBox.Show("Successfully saved...");
                                }
                                else
                                {
                                    MessageBox.Show("Invalid Registeration Number...");
                                }
                            }
                            else
                            {
                                MessageBox.Show("Invali Email...");
                            }
                        }
                        else
                        {
                            MessageBox.Show("Invalid Contact...");
                        }
                    }
                    else
                    {
                        MessageBox.Show("Invalid Last Name...");
                    }
                }
                else
                {
                    MessageBox.Show("Invalid First Name...");
                }
                
            }
        }

        private void PictureBox1_Click(object sender, EventArgs e)
        {
            this.Hide();
            dashboard f = new dashboard();
            f.Show();
        }

        private void MaterialTextBox6_TextChanged(object sender, EventArgs e)
        {

        }

        private void MaterialTextBox5_TextChanged(object sender, EventArgs e)
        {

        }

        private void MaterialTextBox4_TextChanged(object sender, EventArgs e)
        {

        }

        private void MaterialTextBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void MaterialTextBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void Label1_Click(object sender, EventArgs e)
        {

        }

        private void Addstudent_Load(object sender, EventArgs e)
        {

        }

        private void MaterialTextBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar >= 'a' && e.KeyChar <= 'z' || e.KeyChar >= 'A' && e.KeyChar <= 'Z' || e.KeyChar == ' ' || e.KeyChar == 8)
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void MaterialTextBox4_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar >= '0' && e.KeyChar <= '9' || e.KeyChar == 8 || e.KeyChar >= 'a' && e.KeyChar <= 'z' || e.KeyChar >= 'A' && e.KeyChar <= 'Z' || e.KeyChar == '@' || e.KeyChar == '.')
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void MaterialTextBox3_KeyPress(object sender, KeyPressEventArgs e)
        {

            if (e.KeyChar >= '0' && e.KeyChar <= '9' || e.KeyChar == 8)
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void MaterialTextBox5_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar >= '0' && e.KeyChar <= '9' || e.KeyChar == 8||e.KeyChar=='C'||e.KeyChar=='S'||e.KeyChar=='-')
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void MaterialTextBox6_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar >= '5' && e.KeyChar <= '6' || e.KeyChar == 8)
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void MaterialTextBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar >= 'a' && e.KeyChar <= 'z' || e.KeyChar >= 'A' && e.KeyChar <= 'Z' || e.KeyChar == ' ' || e.KeyChar == 8)
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }
    }
}
